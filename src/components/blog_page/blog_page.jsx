import React from "react"

import SquareMsLink from "../square_ms_link/square_ms_link"
import styles from "./blog_page.module.scss"
import RectangleLink from "../rectangle_link/rectangle_link"

const sizeStyle = {
  large: {
    width: "606px",
    height: "606px",
  },
  middle: {
    width: "300px",
    height: "350px",
  },
  small: {
    width: "300px",
    height: "250px",
  },
  normal: {
    width: "402px",
    height: "402px",
  },
}

const BlogPage = props => {
  const size = ["large", "middle", "small", "small", "middle"]
  const linkComponent = n => {
    const post = props.blogPosts[n]["node"]
    return (
      <div key={post.id}>
        <SquareMsLink
          link={`/post/${post.slug}`}
          title={post.title}
          date={post.publishDate}
          url={post.heroImage.file.url}
          category={post.category.category_name}
          style={sizeStyle[size[n]]}
        />
      </div>
    )
  }

  return (
    <div className={styles.container}>
      <div className={styles.header}></div>
      <div className={styles.post_list_container}>
        <div className={styles.post_container}>
          <div className={styles.first_container}>{linkComponent(0)}</div>
          <div className={styles.second_container}>
            <div>{linkComponent(1)}</div>
            <div>{linkComponent(2)}</div>
          </div>
          <div className={styles.third_container}>
            <div>{linkComponent(3)}</div>
            <div>{linkComponent(4)}</div>
          </div>
        </div>
        <div className={styles.post_container}>
          {props.blogPosts.slice(5).map(({ node: post }) => {
            return (
              <div>
                <SquareMsLink
                  link={`/post/${post.slug}`}
                  title={post.title}
                  date={post.publishDate}
                  url={post.heroImage.file.url}
                  category={post.category.category_name}
                  style={sizeStyle["normal"]}
                />
              </div>
            )
          })}
        </div>
      </div>
      <div className={styles.sp_post_list}>
        {props.blogPosts.map(({ node: post }) => {
          return (
            <div className={styles.rectangle_link_container}>
              <RectangleLink
                link={`/post/${post.slug}`}
                title={post.title}
                date={post.publishDate}
                url={post.heroImage.file.url}
                category={post.category.category_name}
              />
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default BlogPage
