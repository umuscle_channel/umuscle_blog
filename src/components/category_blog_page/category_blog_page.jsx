import React from "react"

import SquareMsLink from "../square_ms_link/square_ms_link"
import RectangleLink from "../rectangle_link/rectangle_link"
import styles from "./category_blog_page.module.scss"

const CategoryBlogPage = props => {
  return (
    <>
      <div className={styles.header}></div>
      <div className={styles.container}>
        <div className={styles.post_container}>
          {props.blogPosts.map(({ node: post }) => {
            return (
              <div>
                <SquareMsLink
                  link={`/post/${post.slug}`}
                  title={post.title}
                  date={post.publishDate}
                  url={post.heroImage.file.url}
                  category={post.category.category_name}
                  style={{ width: "402px", height: "402px" }}
                />
              </div>
            )
          })}
        </div>
        <div className={styles.sp_post_container}>
          {props.blogPosts.map(({ node: post }) => {
            return (
              <div>
                <RectangleLink
                  link={`/post/${post.slug}`}
                  title={post.title}
                  date={post.publishDate}
                  url={post.heroImage.file.url}
                  category={post.category.category_name}
                  style={{ width: "402px", height: "402px" }}
                />
              </div>
            )
          })}
        </div>
      </div>
    </>
  )
}

export default CategoryBlogPage
