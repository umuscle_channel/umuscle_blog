import React, { useEffect, useState } from "react"
import axios from "axios"
import styles from "./portfolio.module.scss"

const binance = {
  fil: 243.50478151,
  iost: 274824,
  atom: 160.26866139,
  bnb: 6.19661877,
  sxp: 0.54645863,
}
const metamask = {
  eth: 0.0101,
  usdt: 52.949,
  dai: 30.386,
}

const plasm = {
  eth: 20,
  plm: 793840.15,
}

const uniswap = {
  uni: 400,
}

const compound = {
  usdt: 2922,
  eth: 0.1028,
}

const coincheck = {
  btc: 0.545,
}

const PortfolioPage = props => {
  // const [sum, setSum] = useState(0)
  const [btc, setBtc] = useState(0)
  const [fil, setFil] = useState(0)
  const [iost, setIost] = useState(0)
  const [atom, setAtom] = useState(0)
  const [bnb, setBnb] = useState(0)
  const [sxp, setSxp] = useState(0)
  const [eth, setEth] = useState(0)
  const [dai] = useState(1)
  const [usdt] = useState(1)
  const [plm] = useState(0)
  const [uni, setUni] = useState(0)

  const prices = document.getElementsByClassName("price")

  useEffect(() => {
    const getPrice = async (symbol, setState) => {
      const response = await axios.get(
        `https://api3.binance.com/api/v3/avgPrice?symbol=${symbol}`
      )
      setState(response.data.price)
    }
    getPrice("FILUSDT", setFil)
    getPrice("IOSTUSDT", setIost)
    getPrice("ATOMUSDT", setAtom)
    getPrice("BNBUSDT", setBnb)
    getPrice("SXPUSDT", setSxp)
    getPrice("ETHUSDT", setEth)
    getPrice("UNIUSDT", setUni)
    getPrice("BTCUSDT", setBtc)

    // console.log(prices[0].innerHTML)
    console.log("aa")
  })
  return (
    <>
      <div className={styles.header}></div>
      <div className={styles.container}>
        <h2>取引所</h2>
        <h3>Binance</h3>
        <table className={styles.normal_table}>
          <tbody>
            <tr>
              <td>Filecoin</td>
              <td>{binance.fil}FIL</td>
              <td className="price">{Math.floor(binance.fil * fil)}ドル</td>
            </tr>
            <tr>
              <td>IOST</td>
              <td>274824IOST</td>
              <td className="price">{Math.floor(binance.iost * iost)}ドル</td>
            </tr>
            <tr>
              <td>Cosmos</td>
              <td>160.26866139ATOM</td>
              <td className="price">{Math.floor(binance.atom * atom)}ドル</td>
            </tr>
            <tr>
              <td>BNB</td>
              <td>6.19661877BNB</td>
              <td className="price">{Math.floor(binance.bnb * bnb)}ドル</td>
            </tr>
            <tr>
              <td>SXP</td>
              <td>0.54645863SXP</td>
              <td className="price">{Math.floor(binance.sxp * sxp)}ドル</td>
            </tr>
          </tbody>
        </table>

        <h2>ウォレット</h2>
        <h3>Metamask</h3>
        <table className={styles.normal_table}>
          <tbody>
            <tr>
              <td>Ethereum</td>
              <td>{metamask.eth}Eth</td>
              <td>{Math.floor(metamask.eth * eth)}ドル</td>
            </tr>
            <tr>
              <td>DAI</td>
              <td>{metamask.dai}Dai</td>
              <td>{Math.floor(metamask.dai * dai)}ドル</td>
            </tr>
            <tr>
              <td>USDT</td>
              <td>{metamask.usdt}USDT</td>
              <td>{Math.floor(metamask.usdt * usdt)}ドル</td>
            </tr>
          </tbody>
        </table>

        <h2>ロックアップ</h2>
        <h3>PLASM</h3>
        <table className={styles.normal_table}>
          <tbody>
            <tr>
              <td>Ethereum</td>
              <td>{plasm.eth}Eth</td>
              <td>{Math.floor(plasm.eth * eth)}ドル</td>
            </tr>
            <tr>
              <td>Plasm</td>
              <td>{plasm.plm}plm</td>
              <td>{Math.floor(plasm.plm * plm)}ドル</td>
            </tr>
          </tbody>
        </table>
        <h3>Uniswap</h3>
        <table className={styles.normal_table}>
          <tbody>
            <tr>
              <td>Uni</td>
              <td>{uniswap.uni}uni</td>
              <td>{Math.floor(uniswap.uni * uni)}ドル</td>
            </tr>
          </tbody>
        </table>

        <h2>DeFi</h2>
        <h3>Compound</h3>
        <table className={styles.normal_table}>
          <tbody>
            <tr>
              <td>USDT</td>
              <td>{compound.usdt}Eth</td>
              <td>{Math.floor(compound.usdt * usdt)}ドル</td>
            </tr>
            <tr>
              <td>Ethereum</td>
              <td>{compound.eth}plm</td>
              <td>{Math.floor(compound.eth * eth)}ドル</td>
            </tr>
          </tbody>
        </table>

        <h2>Lending</h2>
        <h3>Coincheck</h3>
        <table className={styles.normal_table}>
          <tbody>
            <tr>
              <td>BTC</td>
              <td>{coincheck.btc}btc</td>
              <td>{Math.floor(coincheck.btc * btc)}ドル</td>
            </tr>
          </tbody>
        </table>
      </div>
      {/* 合計：{sum}ドル */}
    </>
  )
}

export default PortfolioPage
