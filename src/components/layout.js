/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header/header"
import "./layout.css"
import styles from "./layout.module.scss"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
        width: "100%",
      }}
      className={styles.wrapper}
    >
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <main
        style={{
          marginBottom: "40px",
        }}
      >
        {children}
      </main>
      <footer
        style={{
          marginTop: "auto",
          paddingTop: "20px",
          paddingBottom: "40px",
          background: "#f3f3f3",
          textAlign: "center",
          width: "100%",
        }}
      >
        ©Copyright {new Date().getFullYear()}　<strong>馬ッスルブログ</strong>
      </footer>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
