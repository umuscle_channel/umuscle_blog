import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styles from "./nav.module.css"
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
// import { faChevronRight } from "@fortawesome/free-solid-svg-icons"

// const iconStyle = { color: "white", fontSize: "16px" }

const Nav = () => (
  <nav className={styles.navigation}>
    <ul>
      <li>
        {/* <FontAwesomeIcon style={iconStyle} icon={faChevronRight} /> */}
        <Link to={"/"}>TOP</Link>
      </li>
      <li>
        <Link to={"/programming"}>プログラミング</Link>
      </li>
      <li>
        <Link to={"/books"}>読書</Link>
      </li>
      <li>
        <Link to={"/money"}>お金</Link>
      </li>
      <li>
        <Link to={"/others"}>その他</Link>
      </li>
    </ul>
  </nav>
)

export default Nav
