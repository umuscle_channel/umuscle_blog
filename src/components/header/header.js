import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import styles from "./header.module.scss"

const Header = ({ siteTitle }) => (
  <header
    style={{
      width: "100%",
      position: "absolute",
    }}
  >
    <div className={styles.header_container}>
      <div style={{ margin: 0 }}>
        <Link to="/" className={styles.title}>
          {siteTitle}
        </Link>
      </div>
      <div className={styles.links_container}>
        <ul>
          <li>
            <Link to={"/programming"}>プログラミング</Link>
          </li>
          <li>
            <Link to={"/books"}>読書</Link>
          </li>
          <li>
            <Link to={"/money"}>お金</Link>
          </li>
          <li>
            <Link to={"/others"}>その他</Link>
          </li>
        </ul>
      </div>
      <div className={styles.hamburger_menu}>
        <input type="checkbox" id={styles.menu_btn_check} />
        <label htmlFor={styles.menu_btn_check} className={styles.menu_btn}>
          <span></span>
        </label>
        <div className={styles.menu_content}>
          <ul>
            <li>
              <Link to={"/programming"}>プログラミング</Link>
            </li>
            <li>
              <Link to={"/books"}>読書</Link>
            </li>
            <li>
              <Link to={"/money"}>お金</Link>
            </li>
            <li>
              <Link to={"/others"}>その他</Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
