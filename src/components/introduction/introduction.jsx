import React from "react"
import styles from "./introduction.module.scss"
import instagram from "../../images/instagram_icon.png"
import twitter from "../../images/twtter_icon.png"
import standfm from "../../images/standfm_icon.png"

const Introduction = () => {
  return (
    <div className={styles.container}>
      <div className={styles.image_container}>
        <img
          src={
            "https://images.ctfassets.net/rr78kihmmlo5/7crpC6qq4sdA9mtAk3RvzI/f1ee7c405a1d977c6671c37ce4aaf433/S__14835737.jpg"
          }
          alt={"title image"}
        />
      </div>
      <div className={styles.name}>
        左：たかば（アルパカ）
        <br />
        右：アイリスオーヤマン（馬）
      </div>
      <div className={styles.bio}>
        {`このブログの運営は私アイリスオーヤマン担当です。アイリスオーヤマ製品が好き。マッスルグリルも好き。
        たかばは主にラジオや動画の編集。あと料理担当。私のご飯をたまに作ってくれる。後輩のくせに私を見下している。
        `}
      </div>
      <div className={styles.sns}>
        <a href="https://stand.fm/channels/5e9b4363fbbe335df0b530ee">
          <img src={standfm} alt="standfm" />
        </a>
        <a href="https://twitter.com/umuscle_channel">
          <img src={twitter} alt="twitter" />
        </a>
        <a href="https://www.instagram.com/umuscle_channel/">
          <img src={instagram} alt="instagram" />
        </a>
      </div>
    </div>
  )
}

export default Introduction
