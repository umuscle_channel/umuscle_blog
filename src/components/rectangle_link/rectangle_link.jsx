import React from "react"
import styles from "./rectangle_link.module.scss"
import { Link } from "gatsby"

const formatDate = date => {
  var dd = new Date(date)
  var YY = dd.getFullYear()
  var MM = dd.getMonth() + 1
  var DD = dd.getDate()
  return `${YY}年${MM}月${DD}日`
}

const RectangleLink = props => {
  return (
    <Link to={props.link}>
      <div className={styles.container}>
        <div className={styles.image_container}>
          <img src={`https:${props.url}`} alt={"title image"} />
        </div>
        <div className={styles.right_container}>
          <div className={styles.right_top}>
            <div className={styles.date_container}>
              {formatDate(props.date)}
            </div>
            <div className={styles.category_container}>{props.category}</div>
          </div>
          <div className={styles.title_container}>{props.title}</div>
        </div>
      </div>
    </Link>
  )
}

export default RectangleLink
