import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styles from "./post.module.scss"
import "./post.scss"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faClock,
  faSyncAlt,
  faChevronDown,
} from "@fortawesome/free-solid-svg-icons"

const formatDate = date => {
  var dd = new Date(date)
  var YY = dd.getFullYear()
  var MM = dd.getMonth() + 1
  var DD = dd.getDate()
  return `${YY}年${MM}月${DD}日`
}

const BlogPost = ({ data }) => {
  const {
    title,
    heroImage,
    body,
    publishDate,
    author,
    tags,
    updatedAt,
  } = data.contentfulBlogPost

  const tagElements = tags ? (
    tags.map(tag => {
      return (
        <div key={tag.slug} className={styles.tag_container}>
          {tag.title}
        </div>
      )
    })
  ) : (
    <></>
  )

  return (
    <Layout>
      <SEO title={title} />
      {/* <div className={styles.arrow_container}> */}
      <a className={styles.arrow} href="#body_container">
        <FontAwesomeIcon
          className={styles.font_awesome_arrow}
          icon={faChevronDown}
        />
      </a>
      <div className={styles.tag_and_time_container}>
        <div className={styles.date_container}>
          <div>
            <FontAwesomeIcon
              className={styles.font_awesome_create}
              icon={faClock}
            />
            {formatDate(publishDate)}
          </div>
          <div>
            <FontAwesomeIcon
              className={styles.font_awesome_update}
              icon={faSyncAlt}
            />
            {formatDate(updatedAt)}
          </div>
        </div>
        <div className={styles.tags_container}>{tagElements}</div>
      </div>
      <div className={styles.top_container}>
        <img
          alt={title}
          src={heroImage.file.url}
          className={styles.hero_image}
        />
        <div className={styles.top_div_container}>
          <div className={styles.top_titles_container}>
            <h1 className={styles.title}>{title}</h1>
          </div>
        </div>
      </div>
      <div className={styles.container}>
        <div className={styles.body_container} id="body_container">
          <div
            dangerouslySetInnerHTML={{
              __html: body.childMarkdownRemark.html,
            }}
            className={styles.body}
          />
          <div className={styles.author}>
            <div className={styles.author_avatar_container}>
              <img alt={title} src={author.avatar.file.url} />
            </div>
            <div className={styles.author_info_container}>
              <div className={styles.author_name}>{author.name}</div>
              <div className={styles.author_bio}>{author.bio}</div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
export default BlogPost
export const pageQuery = graphql`
  query($slug: String!) {
    contentfulBlogPost(slug: { eq: $slug }) {
      id
      title
      slug
      publishDate
      body {
        childMarkdownRemark {
          html
        }
      }
      heroImage {
        file {
          url
        }
      }
      author {
        name
        bio
        avatar {
          file {
            url
          }
        }
      }
      tags {
        title
        slug
      }
      updatedAt
    }
  }
`
