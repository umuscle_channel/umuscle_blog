import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import CategoryBlogPage from "../components/category_blog_page/category_blog_page"

const Books = ({ data }) => {
  const blogPosts = data.allContentfulBlogPost.edges
  return (
    <Layout>
      <SEO title="読書" />
      <CategoryBlogPage blogPosts={blogPosts} title={"読書"} />
    </Layout>
  )
}

export default Books

export const query = graphql`
  query BookPostPageQuery {
    allContentfulBlogPost(
      filter: { category: { category_name: { eq: "読書" } } }
      limit: 9
      sort: { fields: publishDate, order: DESC }
    ) {
      edges {
        node {
          id
          title
          slug
          publishDate
          category {
            category_name
          }
          heroImage {
            file {
              url
            }
          }
          author {
            name
            avatar {
              file {
                url
              }
            }
          }
        }
      }
    }
  }
`
