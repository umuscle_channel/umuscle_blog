import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import CategoryBlogPage from "../components/category_blog_page/category_blog_page"

const Money = ({ data }) => {
  const blogPosts = data.allContentfulBlogPost.edges
  return (
    <Layout>
      <SEO title="お金" />
      <CategoryBlogPage blogPosts={blogPosts} title={"お金"} />
    </Layout>
  )
}

export default Money

export const query = graphql`
  query MoneyPostPageQuery {
    allContentfulBlogPost(
      filter: { category: { category_name: { eq: "お金" } } }
      limit: 10
      sort: { fields: publishDate, order: DESC }
    ) {
      edges {
        node {
          id
          title
          slug
          publishDate
          category {
            category_name
          }
          heroImage {
            file {
              url
            }
          }
          author {
            name
            avatar {
              file {
                url
              }
            }
          }
        }
      }
    }
  }
`
