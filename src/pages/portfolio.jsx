import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import PortfolioPage from "../components/portfolio_page/portfolio"

const Portfolio = () => {
  return (
    <Layout>
      <SEO title="仮想通貨ポートフォリオ" />
      <PortfolioPage title={"仮想通貨ポートフォリオ"} />
    </Layout>
  )
}

export default Portfolio
