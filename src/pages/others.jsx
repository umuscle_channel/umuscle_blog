import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import CategoryBlogPage from "../components/category_blog_page/category_blog_page"

const Programming = ({ data }) => {
  const blogPosts = data.allContentfulBlogPost.edges
  return (
    <Layout>
      <SEO title="その他" />
      <CategoryBlogPage blogPosts={blogPosts} title={"その他"} />
    </Layout>
  )
}

export default Programming

export const query = graphql`
  query OthersPostPageQuery {
    allContentfulBlogPost(
      filter: { category: { category_name: { eq: "その他" } } }
      limit: 10
      sort: { fields: publishDate, order: DESC }
    ) {
      edges {
        node {
          id
          title
          slug
          publishDate
          category {
            category_name
          }
          heroImage {
            file {
              url
            }
          }
          author {
            name
            avatar {
              file {
                url
              }
            }
          }
        }
      }
    }
  }
`
