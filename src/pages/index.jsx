import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import styles from "./index.module.scss"
import BlogPage from "../components/blog_page/blog_page"

const IndexPage = ({ data }) => {
  const blogPosts = data.allContentfulBlogPost.edges
  return (
    <Layout>
      <SEO title="Home" />
      <BlogPage blogPosts={blogPosts} title={"最近の投稿"} />
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query BlogPostPageQuery {
    allContentfulBlogPost(sort: { fields: publishDate, order: DESC }) {
      edges {
        node {
          id
          title
          slug
          publishDate
          category {
            category_name
          }
          heroImage {
            file {
              url
            }
          }
          author {
            name
            avatar {
              file {
                url
              }
            }
          }
        }
      }
    }
  }
`
